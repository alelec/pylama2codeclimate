from setuptools import setup
from os import path

# Get the long description from the README file
with open(path.join(path.dirname(__file__), 'Readme.rst'), 'r') as f:
    long_description = f.read()

setup(
    name='pylama2codeclimate',
    use_scm_version=True,  # This will generate the version number from git tags
    description='Converts the (parsable) pylama output to codeclimate.json format',
    long_description=long_description,
    url='https://gitlab.com/alelec/pylama2codeclimate',
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    license='MIT',
    py_modules=['pylama2codeclimate'],
    setup_requires=['setuptools_scm'],

    # To provide executable scripts, use entry points keyword. This should point to a function.
    # Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            'pylama2codeclimate=pylama2codeclimate:main',
        ],
    },
)
